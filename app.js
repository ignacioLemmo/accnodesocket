﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var jsonfile = require('jsonfile');
var util = require('util');
var net = require('net');

var socketio = require('socket.io');
var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));



/*-----------REMOTE NODE-------------*/

var identityController = require('./Controllers/identityController.js');
var userManager=require('./Controllers/usersController.js');

var port = process.env.PORT || 80;
var server = app.listen(port, function () {
    console.log('Server listening on', server.address().address , server.address().port);
});

var io = socketio.listen(server);

io.sockets.on("connection", function (socket) {
    
    console.log("cliente conectado");
    socket.on('identify', function (identiy) {
        

       /* if (identiy.from == 'local' && !identityController.searcHomeClients(identiy.homeControlID)) {
            
            console.log("************************************")
            console.log("Nuevo Cliente Local conectado con ID de conexion: "+ identiy.homeControlID)
            console.log("************************************")
            
            identiy.socketID = socket.id
            identityController.setHomeClient(identiy);

        }*/

        if (identiy.from == 'local' && userManager.getUser(identiy.homeControlID) == null) {

            console.log("************************************")
            console.log("Nuevo Cliente Local conectado con ID de conexion: "+ identiy.homeControlID)
            console.log("************************************")

            identiy.socketID = socket.id;
            userManager.addUser(identiy.homeControlID,identiy);
            userManager.addSocketToCache(socket.id,identiy.homeControlID);

        }

        /*if (identiy.from == 'remote' && !identityController.searchAppClients(identiy.appControlID)) {
            
            console.log("************************************")
            console.log("Nuevo Cliente Remoto conectado ID de conexion: " + identiy.appControlID)
            console.log("************************************")

            identiy.socketID = socket.id
            identityController.setAppClient(identiy);


        }*/

        if (identiy.from == 'remote' && userManager.getUser(identiy.appControlID) == null) {

            console.log("************************************")
            console.log("Nuevo Cliente Remoto conectado ID de conexion: " + identiy.appControlID)
            console.log("************************************")

            identiy.socketID = socket.id
            userManager.addUser(identiy.appControlID,identiy);
            userManager.addSocketToCache(socket.id,identiy.appControlID);

        }


       console.log("CLIENTES CONECTADOS:");

        userManager.getUserConnected();

    });



    socket.on('disconnect', function () {

        console.log("Cliente desconectado")
        var userID=userManager.getUserID(socket.id)
        console.log("ID DE USUARIO: ");
        if(userID != null){
            userManager.delete(userID);
            userManager.delete(socket.id);

            console.log("usuario eliminado");
        }

    });

    socket.on('digital-out', function (data) {
        console.log(data);
        var userLocal= userManager.getUser(data.homeControlID);

        if(userLocal){
            io.to(userLocal.socketID).emit('turn-on-off',data);
        }



    });

    socket.on('analog-in',function(data){

        console.log(data);

        var appClient=userManager.getUser(data.to);

        if(appClient){

            console.log(appClient.socketID);

            io.to(appClient.socketID).emit('analog-to-app',data);

        }

    });

    socket.on('check-connection',function(data){

        console.log(data);
        var userLocal= userManager.getUser(data.to);

        if(userLocal){
            io.to(userLocal.socketID).emit('get-devices-connection');
        }

    });

    socket.on('connection-status',function(data){

        console.log(data);
        var appClient=userManager.getUser(data.to);

        if(appClient){
            io.to(appClient.socketID).emit('set-devices-connection',data);
        }

    });
    socket.on('read-tag',function(data){

        console.log(data);
        var appClient=userManager.getUser(data.to);
        
        if(appClient){
            io.to(appClient.socketID).emit('user-tag',data);
        }

    });

})

app.get('/', function (req, res) {
    res.send("acc-connection");
});