(function(user){

    var nodeCache = require( "node-cache" );
    var myCache = new nodeCache({ checkperiod: 0 } );

    user.addUser = function (key,obj) {

        var res=false;

        myCache.set(key,obj,function(err, succes){

            if(!err && succes){

                return res=true;

            }

            else{

                return res;
            }

        });

    }
    
    user.getUser= function (key) {

        var user=null;

        try{
            user = myCache.get( key, true );

            return user;

        } catch( err ){
            return user
        }

    }

    user.getUserID= function (key) {

        var userID=null;

        try{
            userID = myCache.get(key,true);
            return userID;
        }
        catch (err){
            return userID;
        }


    }

    user.getStatus=function(){

        myCache.getStats();

    }
    
    user.getUserConnected= function () {
        myCache.keys( function( err, mykeys ){
            if( !err ){
                console.log( mykeys );

            }
            else
            {
                console.log("error al obtener IDs")
            }
        });
    }
    
    user.delete= function (key) {

        var resul=false;

        myCache.del( key, function( err, count ){
            if( !err ){
                console.log("Claves encontradas "+ count );
                resul=true;
            }

            return resul;

        });

    }

    user.addSocketToCache=function(key, id){

        var res=false;

        myCache.set(key,id,function(err, succes){

            if(!err && succes){

                return res=true;

            }

            else{

                return res;
            }

        });

        user.addDevice=function(key,device){

            var user=null;

            try{
                user = myCache.get(key,true);

                console.log("Dispositivos encontrados: "+user.devices);

            }
            catch (err){
                console.log("usuario no encontrado");
            }

        }

    }



})(module.exports);
