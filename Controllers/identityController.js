﻿(function (indentity) { 

    var _homeClients = [];
    var _remoteClients = [];


    indentity.getHomeClientByID=function(homeID){

        var homeClient={};

        console.log("cantidad de clientes locales :"+_homeClients.length)

        for (var i = 0; i < _homeClients.length; i++) {
            if (_homeClients[i].homeControlID == homeID) {

                console.log("cliente econtrado: "+_homeClients[i].homeControlID);
                console.log("sokcetID: "+_homeClients[i].socketID);

                homeClient=_homeClients[i];

            }
        }

        return homeClient;

    }

    indentity.getAppClientByID=function(appID){

        var remoteClient={};

        console.log("cantidad de clientes remotos :"+_remoteClients.length)

        for (var i = 0; i < _remoteClients.length; i++) {
            if (_remoteClients[i].appControlID == appID) {

                console.log("cliente econtrado: "+_remoteClients[i].appControlID);
                console.log("sokcetID: "+_remoteClients[i].socketID);

                remoteClient=_remoteClients[i];

            }
        }

        return remoteClient;

    }
        
    indentity.searcHomeClients=function(homeID) {
        
        
        if (_homeClients.length == 0) {
            return false;
        }
        
        for (var i = 0; i < _homeClients.length; i++) {
            if (_homeClients[i].homeControlID == homeID) {
                return true;
            }
        }

    }
    
    indentity.searchAppClients=function(appID) {
        
        if (_remoteClients.length == 0) {
            return false;
        }
        
        for (var i = 0; i < _remoteClients.length; i++) {
            if (_remoteClients[i].appControlID == appID) {
                return true;
            }
        }

    }
    
    
   function getIndexOfsocketIDOnHomeClients(socketID) {
        
        var index = -1;
        
        for (var i = 0; i < _homeClients.length; i++) {
            if (_homeClients[i].socketID == socketID) {
                index = i
            }
        }
        
        return index;
    }
    
   function getIndexOfsocketIDOnRemoteClients(socketID) {
        
        var index = -1;
        
        for (var i = 0; i < _remoteClients.length; i++) {
            if (_remoteClients[i].socketID == socketID) {
                index = i
            }
        }
        
        return index;
    }


    indentity.disconnectClient = function (socketId) { 
    
        var indexSocketIDHomeClients = getIndexOfsocketIDOnHomeClients(socketId)
        var indexSocketIDRemoteClients = getIndexOfsocketIDOnRemoteClients(socketId)
        
        if (indexSocketIDHomeClients > -1) {
            
            _homeClients.splice(indexSocketIDHomeClients, 1)

            return true

        }
        
        
        if (indexSocketIDRemoteClients > -1) {
            
            _remoteClients.splice(indexSocketIDRemoteClients, 1)

            return true


        }
    
        return false

    }

    indentity.setHomeClient = function (identiy) {
      
        _homeClients.push(identiy);

    }


    indentity.setAppClient = function (identiy) {
        
        _remoteClients.push(identiy);

    }

    indentity.getAppClient = function () {
        
        return _remoteClients;

    }

    indentity.getHomeClient = function () {
        
        return _homeClients;

    }


    indentity.disconnectHomeClient = function (homeID) { 
    
        for (var i = 0; i < _homeClients.length; i++) {
            if (_homeClients[i].homeControlID == homeID) {
                return i;
            }
        }

        return -1;
    
    }

    indentity.removeHomeClient = function (index) { 
    
        _homeClients.splice(index, 1)
    
    };

       /* indentity.getIndexOfsocketIDOnRemoteClients=function (socketID) {
        
        var index = -1;
        
        for (var i = 0; i < _remoteClients.length; i++) {
            if (_remoteClients[i].socketID == socketID) {
                index = i
            }
        }
        
        return index;
    }
    
    indentity.getIndexOfsocketIDOnHomeClients=function (socketID) {
        
        var index = -1;
        
        for (var i = 0; i < _homeClients.length; i++) {
            if (_homeClients[i].socketID == socketID) {
                index = i
            }
        }
        
        return index;
    }*/



})(module.exports)