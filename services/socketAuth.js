﻿(function (socketAuth) {
    
    //var http = require('http');
    var request = require('request');
    var Q = require('q');


    socketAuth.getHomeControlID = function (host,username) {
        
        var data = {};
        var deferred = Q.defer();

        request(host+'/api/'+username+'/socketauth', function (error, response, body) {
            
            if (error) { 
                deferred.reject(error)
            }
            
            if (response.statusCode !== 200) {
                deferred.reject(response)
            }

            if (!error && response.statusCode == 200) {
                deferred.resolve(body)
            }
        })
        
        return deferred.promise;
    }

})(module.exports);